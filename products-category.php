<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/src/style.css">
    <link rel="stylesheet" href="/src/style.css.map">
</head>
<body>
<?
require 'template/header.php';
?>
<main class="main inner">
    <div class="container">
        <div class="sidebar">
<!--            <a class="sidebar__back" href="/">-->
<!--                Каталог продукции-->
<!--            </a>-->
    <?php
    error_reporting(E_ALL);
    ini_set('display_errors', 'on');
    $productAr =[];
    $categoryT = htmlspecialchars($_GET["category"]);
    $categoryInner = htmlspecialchars($_GET["products"]);
    $page = htmlspecialchars($_GET["page"]);
    $sxml = simplexml_load_file("tree.xml");
    foreach ($sxml -> page-> page  as $product) {
        if((int)$product->page["parent_page_id"] == (int)$categoryT) {
            $categoryOut = $product-> name;
            echo '<a class="sidebar__back" href="/products.php?category='.$categoryT.'&page=0">'. $categoryOut . '</a>'; ?>
            <div class="sidebar__wrapper">
                <a class="sidebar__item">Выберете категорию</a>
            <?
            foreach ($product->page as $item) {
                echo '<a class="sidebar__item" href="/products-category.php?category='.$categoryT.'&products='.$item->page_id.'&page=0">'. $item->name .'</a>';
                if((int)$item->product->page == (int)$categoryInner) {
                    $categoryMain = $item->name;
                    foreach ($item->product as $productItem) {
                        array_push($productAr, (int)$productItem->product);
                    }
                }
            }
            ?></div>
                <?
        }

    }
    $productAr = array_chunk($productAr, 18);
//    echo '<pre>';
//    var_dump($productAr);
//    echo '</pre>';
    ?>
            <a class="sidebar__back-mobile"><?=$categoryMain ?></a>
</div>
        <script>
            document.addEventListener("DOMContentLoaded", function(event) {
                document.querySelector('.sidebar__wrapper').addEventListener('click', function(){
                    this.classList.toggle('active');
                    arItem = document.querySelectorAll('.sidebar__item');
                    arItem.forEach(function(entry) {
                        entry.classList.toggle('active');
                    });
                });

            });
        </script>
        <section class="goods">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" class="item__link">Каталог \</a></li>
                <li class="breadcrumbs__item"><a href="/products.php?category=<?=$categoryT ?>&page=0" class="item__link"><?= $categoryOut ?></a></li>
                <li class="breadcrumbs__item"><a class="item__link"><?=$categoryMain ?></a></li>

            </ul>
<?
//Конвертируем XML-файл в объект
$sxml = simplexml_load_file("pr.xml");

foreach ($sxml -> product  as $product) {
    $countProduct = 0;
    if(in_array((int)$product->product_id, $productAr[$page])) {
        $countProduct++;
       echo '<a class="goods__item" href="/product.php?id='. $product-> product_id.'&products='.$categoryInner.'&category='.$categoryT.'">';
        ?>
        <h3 class="item__name"><?echo $product -> name ?></h3>
        <img src="/<?= $product -> small_image->attributes() ?>" alt="">
        <div class="item__price"><? echo $product-> price-> price;?></div>
        <div class="item__sku"><?echo 'Артикул: '. $product -> code ;?></div>
        <? if( $product-> brand) { ?> <div class="item__brand"><?echo 'Брэнд: '. $product-> brand ;?></div> <? } ?>
        </a>
        <?
    }
}
echo $countProduct ? '<h1>нет подходящих товаров</h1>': false;
// curl --retry-delay 0 --output pr.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/product.xml'
// curl --retry-delay 0 --output treew.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/treeWithoutProducts.xml'
?>
    <div class="pagination">
        <?
        $url = $_SERVER['REQUEST_URI'];
        $url = explode('?', $url);
        $url = $url[0];
        $currentPage;
        ?>
        <?
        if ($page == count($productAr)-1) {
            ?>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page-2; ?>"><? echo $currentPage = $page-1; ?></a>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page-1; ?>"><? echo $currentPage = $page; ?></a>
            <a class="pagination__item active"><? echo $page+1 ?></a>
            <?
        } elseif ($page == 0) { ?>
            <a class="pagination__item active" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $page ?>"><? echo $page+1 ?></a>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page+1 ?>"><? echo $page+2 ?></a>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page+2 ?>"><? echo $page+3 ?></a>
            <?
        } else { ?>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page-1; ?>"><? echo $page ?></a>
            <a class="pagination__item active" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page ;?>"><? echo $page+1 ?></a>
            <a class="pagination__item" href="<? echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $currentPage = $page+1; ?>"><? echo $page+2 ?></a>
            <?
        }
        ?>
    </div>

<!--<section class="pagination">-->
<!--    <h2>ПАГИНАЦИЯ БЛЯТЬ</h2>-->
<!--    --><?//
//    $url = $_SERVER['REQUEST_URI'];
//    $url = explode('?', $url);
//    $url = $url[0];
//    ?>
<!--    <a href="--><?// echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $page-- ?><!--">--><?// echo $page ?><!--</a>-->
<!--    <a href="--><?// echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $page ?><!--">--><?// echo $page++ ?><!--</a>-->
<!--    <a href="--><?// echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $page++ ?><!--">--><?// echo $page+2 ?><!--</a>-->
<!--  --><?//
//    for($i = 0; $i <= count($productAr)-1; $i++) {
//        ?>
<!--        <a href="--><?// echo $url .'?category='.$categoryT.'&products='.$categoryInner.'&page='. $i ?><!--">--><?// echo $i+1 ?><!--</a>-->
<!--        --><?//
//    }
//    ?>
<!--</section>-->
</section>
</main>
<?
include 'template/footer.php';
?>
</body>
</html>