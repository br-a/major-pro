<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/src/style.css">
    <link rel="stylesheet" href="/src/style.css.map">
</head>
<body>
<?
require 'template/header.php';
?>
<main class="main contacts">
    <div class="container">
        <div class="contacts__info">
        <h1 class="contacts__title">
            Контакты
        </h1>
            <div class="contacts__tel">
                <h2 class="tel__title">Телефоны</h2>   
                <ul class="tel__list">
                    <li class="tel__item"><a href="tel:74959616638" class="item__link">+7 (495) 961-66-38</a></li>
                    <li class="tel__item"><a href="tel:79269813733" class="item__link">+7 (926) 981-37-33</a></li>
                    <li class="tel__item"><a href="tel:79259253428" class="item__link">+7 (925) 925-34-28</a></li>
                </ul>
            </div>
            <div class="contacts__email">
                <h2 class="email__title">Email</h2>
                <a href="" class="email__link">info@majorpro.ru</a>
            </div>
            <div class="contacts__address">
                <h2 class="address__title">Адрес</h2>
                <div class="address__value">
                МО, Одинцовский гор. округ, д. Переделки, кот. пос. Стольное, д. 2 (3 км от ст. м. Боровское шоссе)
                </div>
            </div>
        </div>
        <div class="contacts__map">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A0f41af0384ac46f72950036aacc5de40ea090582c595516b217e34dc6bba0ee6&amp;width=100%&amp;height=400&amp;lang=ru_RU&amp;scroll=true"></script>
            </div>
        </div>
</main>

<?
include 'template/footer.php';
?>
</body>
</html>