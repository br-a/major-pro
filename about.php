<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/src/style.css">
    <link rel="stylesheet" href="/src/style.css.map">
</head>
<body>
<?
require 'template/header.php';
?>
<main class="main about">
    <div class="container">
        <section>
    <h2>МЫ— РЕКЛАМНО ПРОИЗВОДСТВЕННАЯ КОМПАНИЯ.</h2>
Работаем на собственном оборудовании с профессиональной командой специалистов с 2001 года.
</section>
<section>
<h2>НАША ОСНОВНАЯ ДЕЯТЕЛЬНОСТЬ:</h2>
Сувенирная продукция
Производство полиграфической продукции (упаковка, календари, POS-материалы и др.)
Швейное производство
Шелкотрафаретное производство 
Сублимационная печать
Лазерная резка
</section>
<section>
<h2>С НАМИ РАБОТАЮТ:</h2>
Крупные коммерческие и некоммерческие компании, государственный структуры, рекламные и коммуникационные агентства Москвы и других городов РФ.
</section>
<section>
<h2>УСЛУГИ:</h2>
<ul>
  <li>Сувенирная продукция</li>
  <li>Полиграфическая продукция </li>
  <li>Швейное производство</li>
  <li>Сублимация</li>
  <li>Лазерная резка</li>
  <li>Ремонт и дизайн интерьера</li>
  <li>Огранизация мероприятий</li>
  </ul>
  </section>
    </div>
</main>

<?
include 'template/footer.php';
?>
</body>
</html>