<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/src/style.css">
    <link rel="stylesheet" href="/src/style.css.map">
</head>
<body>

<!--  название всех товаров  --><?php
//    //Конвертируем XML-файл в объект
//    $sxml = simplexml_load_file("pr.xml");
//
//    foreach ($sxml -> product  as $product) {
//        echo $product -> name . '<br>';
//    }
//    // curl --retry-delay 0 --output pr.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/product.xml'
//    // curl --retry-delay 0 --output treew.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/treeWithoutProducts.xml'
//    ?>
<?
    require 'template/header.php';
?>
<main class="main">
    <div class="container category">
        <h1 class="title">Каталог продукции</h1>
<?php

    $cateryAr = [];
    //Конвертируем XML-файл в объект
    $sxml = simplexml_load_file("tree.xml");
    //добавляем элементы категорий в массив
    foreach ($sxml -> page-> page  as $product) {
//        echo $product->name. '<br>';
        array_push($cateryAr, array(
            'name' => $product->name,
            'id' => $product-> page_id
        ));

    }
    $cateryAr = array_chunk($cateryAr, (count($cateryAr)/3));
//    var_dump($cateryAr);
    foreach ($cateryAr as $category) {
        echo '<ul class="category__list">';
        foreach ($category as $item) {
            echo '<li class="list__item"><a class="item__link" href="products.php?category=' . $item['id'] . '&page=0"/>'. $item['name'] . '</a></li>';
        }
        echo '</ul>';
    }
?>
    </div>
</main>
<?
    include 'template/footer.php';
?>



<!--    Описание состава выгрузки-->
<!--    Экспорт содержит следующие xml-файлы + файлы картинок и доп.файлов.-->
<!--    1) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/product.xml содержит только описания товаров-->
<!--    2) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/catalogue.xml содержит описания товаров и структуру каталога сайта gifts.ru (фактически получается конкатенацией файлов product.xml и tree.xml)-->
<!--    3) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/stock.xml содержит информацию о наличии товара на складе-->
<!--    4) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/tree.xml содержит структуру каталога с товарами-->
<!--    5) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/treeWithoutProducts.xml содержит структуру каталога без товаров-->
<!--    6) https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/filters.xml содержит справочник фильтров (информация о том, какие фильтры применимы к какому товару, отдается в product.xml — /product/filters)-->
<!--    7) Файлы картинок и доп.файлов https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/[относительный путь до файла, который указан в product.xml]-->
<!---->
<!--    Чтобы избежать повторного выкачивания картинок и доп.файлов, достаточно анализировать их путь, т.к. он уникален. Т.е. если картинка обновилась, то у нее обязательно обновится и путь (точнее, будет сгенерировано новое имя файла).-->
<!---->
<!--    Структура XML-файлов-->
<!--    1) product.xml-->
<!--    /doct/product — описание товара-->
<!--    product_id — id товара-->
<!--    group — id группировки - опциональное поле (есть только у товаров, которые могут быть объединены в группу - например, разные цвета одной модели футболки)-->
<!--    code — артикул-->
<!--    name — наименование-->
<!--    product_size — размеры предмета-->
<!--    matherial — материал-->
<!--    small_image[@src] — относительный путь к файлу картинки 200х200 (полный адрес: https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/[относительный путь])-->
<!--    big_image[@src] — устаревшее c 22.03.2018!!! - картинки 280 более не создаются, вместо них в качестве заглушки отдаётся путь small_image - относительный путь к файлу картинки 280х280 (полный адрес: https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/[относительный путь])-->
<!--    super_big_image[@src] — относительный путь к файлу картинки в максимальном размере, как правило, 1000х1000 (полный адрес: https://ваш-логин:ваш-пароль@api2.gifts.ru/export/v2/catalogue/[относительный путь])-->
<!--    content — описание html-->
<!--    status — статус товара (status[@id] — id-статуса — 0 — новинка,1 — обычный, 2 — до исчерпания складских остатков, 3 — закрыт)-->
<!--    brand — бренд-->
<!--    barcode — штрих-код-->
<!--    weight — вес (г)-->
<!--    pack — упаковка-->
<!--    amount — количество в упаковке-->
<!--    weight — вес в упаковке (г)-->
<!--    volume — объём упаковки (см3)-->
<!--    sizex, sizey, sizez — ШxВxГ (см)-->
<!--    filters — применимые к данному артикулу фильтры (справочник фильтров в файле filters.xml)-->
<!--    filter/filtertypeid — id применимого типа фильтра-->
<!--    filter/filterid — id применимого фильтра данного типа-->
<!--    ondemand— возможность поставки под заказ (true|false)-->
<!---->
<!--    /doct/product/product — субтовары (размеры текстиля) конкретного артикула:-->
<!--    product_id — id субтовара-->
<!--    code — артикул-->
<!--    name — наименование-->
<!--    barcode — штрих-код-->
<!--    size_code — код размера для текстиля-->
<!--    weight — вес (г)-->
<!---->
<!--    //product/price — доступные цены на товар-->
<!--    price — цена, валюта указана в /doct/product/currency-->
<!--    currency — валюта, в которой указывается цена-->
<!--    name — название-->
<!---->
<!--    //product/print — виды нанесения-->
<!--    name — код вида нанесения (например "А1")-->
<!--    description — название вида нанесения (например "Шелкография")-->
<!---->
<!--    //product/product_attachment — доп.файлы и картинки-->
<!--    meaning — 1 - картинка, 0 - НЕ картинка-->
<!--    file — URL доп.файла-->
<!--    image — URL доп.картинки-->
<!--    name — описание доп.файла или картинки-->
<!---->
<!--    //product/alerts — алерты по артикулу-->
<!--    alert — текст алерта-->
<!---->
<!--    2) catalogue.xml, tree.xml, treeWithoutProducts.xml-->
<!--    /doct//page — дерево страниц каталога товаров ()-->
<!--    page_id — id страницы-->
<!--    name — название страницы-->
<!--    uri — основная часть URL-адреса страницы секции на gifts.ru-->
<!---->
<!--    3) только в catalogue.xml и tree.xml-->
<!--    //page/product — товар на странице-->
<!--    product — product_id товара из product.xml-->
<!--    page — id страницы с товаром-->
<!---->
<!--    4) stock.xml (обновляется каждый час)-->
<!--    /doct/stock — складские остатки по отдельному товару-->
<!--    product_id — product_id товара из product.xml-->
<!--    code — код артикула товара из product.xml-->
<!--    amount — всего на складе-->
<!--    free — доступно для резервирования-->
<!--    inwayamount — всего в пути (поставка)-->
<!--    inwayfree — доступно для резервирования из поставки-->
<!--    enduserprice — цена End-User-->
<!--    dealerprice — цена для дилера-->
<!--    enduserNextprice — цена End-User в будущем прайс-листе *-->
<!--    nextpricebegindate — дата вступления в силу новой цены *-->
<!---->
<!--    * - поле присутствует только у артикулов, по которым запланировано изменение цены-->
<!---->
<!---->
<!--    5) filters.xml-->
<!--    //root/filtertypes — справочник типов фильтров-->
<!--    filtertype — конкретный тип фильтра-->
<!--    /filtertype/filtertypeid — id типа фильтра-->
<!--    /filtertype/filtertypename — название типа фильтра-->
<!--    /filtertype/filters — справочник фильтров для данного типа фильтра-->
<!--    filter/filterid — id фильтра данного типа-->
<!--    filter/filtername — название фильтра данного типа-->
<!---->
<!--    6) complects.xml-->
<!--    /complect — комплект-->
<!--    id — id комплекта-->
<!--    /parts — состав набора-->
<!--    /parts/part — элемент набора-->
</body>
</html>