<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/src/style.css">
    <link rel="stylesheet" href="/src/style.css.map">
</head>
<body>
<?
require 'template/header.php';
?>
<main class="main inner">
    <div class="container">
        <div class="sidebar">
            <!--            <a class="sidebar__back" href="/">-->
            <!--                Каталог продукции-->
            <!--            </a>-->
            <?php
            error_reporting(E_ALL);
            ini_set('display_errors', 'on');
            $productAr =[];
            $categoryT = htmlspecialchars($_GET["category"]);
            $categoryInner = htmlspecialchars($_GET["products"]);
            $productId = htmlspecialchars($_GET["id"]);
            $sxml = simplexml_load_file("tree.xml");
            foreach ($sxml -> page-> page  as $product) {
                if((int)$product->page["parent_page_id"] == (int)$categoryT) {
                    $categoryOut = $product-> name; ?>
            <div class="sidebar__wrapper">
                <a class="sidebar__item">Выберете категорию</a>
                    <?
                    echo '<a class="sidebar__back" href="/products.php?category='.$categoryT.'&page=0">'. $categoryOut . '</a>';
                    foreach ($product->page as $item) {
                        echo '<a class="sidebar__item" href="/products-category.php?category='.$categoryT.'&products='.$item->page_id.'&page=0">'. $item->name .'</a>';
                        if((int)$item->product->page == (int)$categoryInner)
                            $categoryMain = $item->name;
                        foreach ($item ->product as $productItem) {
                            array_push($productAr, (int)$productItem->product);
                        }
                    } ?>
            </div>
                <?
                }

            }
            $productAr = array_chunk($productAr, 18);
            //    echo '<pre>';
            //    var_dump($productAr);
            //    echo '</pre>';
            ?>
            <a class="sidebar__back-mobile"><?=$categoryMain ?></a>
        </div>
        <script>
            document.addEventListener("DOMContentLoaded", function(event) {
                document.querySelector('.sidebar__wrapper').addEventListener('click', function(){
                    this.classList.toggle('active');
                    arItem = document.querySelectorAll('.sidebar__item');
                    arItem.forEach(function(entry) {
                        entry.classList.toggle('active');
                    });
                });
                document.querySelector('.item__order').addEventListener('click', function(){
                    document.querySelector('.modalbuy').classList.add('active');
                });
                document.querySelector('.modalbuy .close').addEventListener('click', function(){
                    document.querySelector('.modalbuy').classList.remove('active');
                });

            });
        </script>
        <section class="goods item">
            <ul class="breadcrumbs">
                <li class="breadcrumbs__item"><a href="/" class="item__link">Каталог \</a></li>
                <li class="breadcrumbs__item"><a href="/products.php?category=<?=$categoryT ?>&page=0" class="item__link"><?= $categoryOut ?> \</a></li>
                <li class="breadcrumbs__item"><a href="/products-category.php?category=<?=$categoryT ?>&products=<?=$categoryInner ?>&page=0" class="item__link"><?=$categoryMain ?></a></li>
            <?
            //Конвертируем XML-файл в объект
            $sxml = simplexml_load_file("pr.xml");

            foreach ($sxml -> product  as $product) {
                if((int)$productId == $product->product_id) {
                    ?>
            </ul>
            <div class="item__wrapper">
            <img class="item__img" src="/<?= $product -> small_image->attributes() ?>" alt="">
                <div class="item__info">
                    <h1 class="item__name"><?= $product -> name ?></h1>
                    <button class="item__order">
                        Оставить заявку
                    </button>
                    <div class="item__desc">Описание: </div>
                    <?
                    if($product -> matherial) {
                            echo '<p>Материал: ' . $product -> matherial .'</p>';
                        }
                    if($product -> brand) {
                        echo '<p>Брэнд: ' . $product -> brand .'</p>';
                        }
                    if($product -> product_size) {
                        echo '<p>Доступные размеры: ' . $product -> product_size .'</p>';
                        }
                    if($product -> weight) {
                        echo '<p>Вес: ' . $product -> weight .'</p>';
                    }
                    if($product -> content) {
                        echo '<p>' . $product -> content .'</p>';
                    }
                    ?>
                </div>
                    </div>
                    <?
                }
            }
            // curl --retry-delay 0 --output pr.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/product.xml'
            // curl --retry-delay 0 --output treew.xml 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/treeWithoutProducts.xml'
            ?>
        </section>
</main>
<?
include 'template/footer.php';
?>
</body>
</html>

<div class="modalbuy">
    <form class="modalbuy__form" method="POST" action="send.php"> 
        <div class="close"></div>
        <h2 class="modalbuy__title">
            оставить заявку
        </h2>
        <div class="modalbuy__text">
            Заполните поля формы и наши сотрудники свяжутся с вами
            в ближайшее время для уточнения деталей
        </div>
        <input name="name" placeholder="Ваше Имя" type="text" class="modalbuy__name">
        <input id="online_phone"
         maxlength="50"
         required="required"
         pattern="\+7\s?[\(]{0,1}9[0-9]{2}[\)]{0,1}\s?\d{3}[-]{0,1}\d{2}[-]{0,1}\d{2}"
        name="phone" placeholder="Ваш Телефон" type="tel" class="modalbuy__tel">
        <input name="email" placeholder="Ваш Email" type="email" class="modalbuy__email">
        <textarea name="text" placeholder="Опишите ваши пожелания и необходимое количество" id=""></textarea>
        <button class="modalbuy__btn">Отправить</button>
    </form>
</div>

<?//
//include 'template/footer.php';
//?>
<!--</body>-->
<!--</html>-->
<!--https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/thumbnails/01159280_1.tif_200x200.jpg-->
<!---->
<!--curl --retry-delay 0 --output 321.jpg 'https://18467_xmlexport:ZyBYdWde@api2.gifts.ru/export/v2/catalogue/thumbnails/01159280_1.tif_200x200.jpg'-->