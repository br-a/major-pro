<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="header__address">
            МО, Одинцовский гор. округ, д. Переделки, кот. пос. Стольное, д. 2
            </div>
            <a href="" class="header__tel">
            +7 (495) 961-66-38
            </a>
        </div>
    </div>
    <menu class="header__menu">
        <?
            include 'menu.php';
        ?>
    </menu>
</header>

<script>
    document.addEventListener("DOMContentLoaded", function(event) {
        document.querySelector('.burger').addEventListener('click', function(){
            document.querySelector('.modal').classList.add('active');
        });
        document.querySelector('.close span').addEventListener('click', function(){
            document.querySelector('.modal').classList.remove('active');
        });
    });
    // document.querySelector('.menu').classList.toggle("animate");
</script>